import 'package:flutter_test/flutter_test.dart';
import 'package:mobiplay/scenes/scene_state.dart';

void main() {
  group("SceneState should", () {
    test("be cloned properly", () {
      final symbol1 = const Symbol("one");
      final symbol2 = const Symbol("two");
      final state = SceneState(scene: symbol1, store: symbol2);
      final result = state.clone();
      expect(result.scene, symbol1);
      expect(result.store, symbol2);
      expect(result != state, true);
      final symbol3 = const Symbol("three");
      final result2 = state.clone(scene: symbol3);
      expect(result2 != state, true);
      expect(result2.store, symbol2);
      expect(result2.scene, symbol3);
      final result3 = state.clone(store: symbol3);
      expect(result3 != state, true);
      expect(result3.store, symbol3);
      expect(result3.scene, symbol1);
    });
  });
}
