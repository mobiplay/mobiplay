import 'package:mobiplay/activities/activity.dart';
import 'package:mobiplay/application/application.dart';
import 'package:mobiplay/application/old_interactor.dart';
import 'package:mobiplay/events/event.dart';
import 'package:mobiplay/services/services.dart';
import 'package:mobiplay/stores/stores.dart';
import 'package:rxdart/rxdart.dart';

class TestApplication extends Application {
  TestApplication(
      {OldInteractor interactor,
      Stores stores,
      PublishSubject<Event> channel,
      Services services})
      : super(
            interactor: interactor,
            stores: stores,
            channel: channel,
            services: services);

  @override
  done(Activity activity) {}
}
