import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mobiplay/ui/screen_ruler.dart';
import 'package:provider/provider.dart';

extension BuildContextScreenRullerExtensions on BuildContext {
  T provide<T>() => Provider.of<T>(this, listen: false);

  ScreenRuler get screenRuler => ScreenRuler.of(this);

  ThemeData get theme => Theme.of(this);

  void clearFocus() => FocusScope.of(this).requestFocus(FocusNode());
}
