import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:mobiplay/application/old_interactor.dart';
import 'package:mobiplay/extensions/build_context_extensions.dart';

class WidgetModel extends ChangeNotifier {
  final OldInteractor appInteractor;
  final BuildContext context;
  WidgetModel(this.context)
      : this.appInteractor = context.provide<OldInteractor>();
}
