import 'package:flutter/cupertino.dart';
import 'package:mobiplay/models/widget_model.dart';

typedef M ModelBuilder<M extends WidgetModel>(BuildContext context);
