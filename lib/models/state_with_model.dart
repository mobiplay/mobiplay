import 'package:mobiplay/models/widget_model.dart';
import 'package:flutter/widgets.dart';

abstract class StateWithModel<M extends WidgetModel, T extends StatefulWidget> extends State<T> {
  M model;
}
