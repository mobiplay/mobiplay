import 'package:flutter/widgets.dart';
import 'package:mobiplay/models/widget_model.dart';
import 'package:provider/provider.dart';

class WidgetModelProvider<T extends WidgetModel> extends StatelessWidget {
  final T Function(BuildContext context) modelBuilder;

  final Widget Function(BuildContext context, T model) widgetBuilder;

  WidgetModelProvider(
      {@required this.widgetBuilder, @required this.modelBuilder});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<T>(
      create: (BuildContext context) => modelBuilder(context),
      child: Consumer<T>(
        builder: (BuildContext context, T model, Widget child) =>
            widgetBuilder(context, model),
      ),
    );
  }
}
