import 'package:mobiplay/providers/provider.dart';
import 'package:mobiplay/stores/provides_state.dart';
import 'package:mobiplay/stores/store.dart';

import 'locates_providers_of_state.dart';

class Stores extends ProviderOf<Store> implements LocatesProviderOfState {

  @override
  ProvidesState<T> locate<T>(Symbol symbol) {
      if (items.containsKey(symbol)){
          return (items[symbol] as Store) as ProvidesState<T>;
      } else return null;
  }
}
