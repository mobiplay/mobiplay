import 'package:flutter_riverpod/all.dart';

abstract class Store<T> extends StateNotifier<T> {
  Store(T state) : super(state);
}
