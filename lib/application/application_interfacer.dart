import 'package:flutter_riverpod/all.dart';

import 'interactor.dart';

abstract class ApplicationInterfacer {
  Interactor get interactor;
  ProviderContainer get scope;
}
