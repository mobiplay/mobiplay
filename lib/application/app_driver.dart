import 'package:flutter_riverpod/all.dart';
import 'package:mobiplay/events/event.dart';
import 'package:rxdart/rxdart.dart';
import 'package:dart_extras/dart_extras.dart';

abstract class ApplicationDriver {
  PublishSubject<Event> get appBus;
  PublishSubject<Event> get inputBus;
  ProviderContainer get scope;
  Future<Result> close();
}
