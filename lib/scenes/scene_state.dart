
import 'package:dart_extras/dart_extras.dart';

class SceneState implements Cloneable {
  static final SceneState empty = SceneState();

  final Symbol store;
  final Symbol scene;

  SceneState({
    Symbol store,
    Symbol scene,
  })  : this.store = store ?? const Symbol(""),
        this.scene = scene ?? const Symbol("");

  @override
  SceneState clone({
    Symbol store,
    Symbol scene,
  }) =>
      SceneState(store: store ?? this.store, scene: scene ?? this.scene);
}
