
import 'package:flutter/widgets.dart';
import 'package:mobiplay/models/state_with_model.dart';
import 'package:mobiplay/models/widget_model.dart';

abstract class SceneWidgetState<M extends WidgetModel, T extends StatefulWidget> extends StateWithModel<M, T > {}
