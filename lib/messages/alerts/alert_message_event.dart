import 'package:mobiplay/events/event.dart';
import 'package:mobiplay/messages/alerts/alert_message.dart';

class AlertMessageEvent extends Event {
  static const Symbol SYMBOL = const Symbol("alert_message_event");
  final AlertMessage message;

  AlertMessageEvent(this.message) : super(SYMBOL);
}
