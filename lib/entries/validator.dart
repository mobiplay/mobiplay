
import 'package:mobiplay/entries/validation.dart';

abstract class Validator<T> {
    Validation validate(T value);
}
